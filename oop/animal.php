<?php

class animal{
    public $name;
    public $legs = 4;
    public $cool_blooded = "no";

    public function __construct($nama){
        $this->name = $nama;
    }
}

/* 
//menggunakan method get
class Animal
{
    public $name = "";
    public $legs = 4 ;
    public $cold_blooded = false;

    public function __construct($name){
        return $this->name = $name;
    }

    public function getName(){
        return $this->name. "<br>";
    }

    public function getLegs(){
        return $this->legs. "<br>";
    }

    public function getCold_Blooded(){
        return (boolval($this->cold_blooded)? 'yes<br>' : 'no<br>');
    }
}
*/
?>