<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new animal("shaun");
echo "Name: " . $sheep->name . "<br>";
echo "legs: " . $sheep->legs . "<br>";
echo "cool blooded: " . $sheep->cool_blooded . "<br>"."<br>";

$kodok = new frog("buduk");
echo "Name: " . $kodok->name . "<br>";
echo "legs: " . $kodok->legs . "<br>";
echo "cool blooded: " . $kodok->cool_blooded . "<br>";
echo "jump : " . $kodok->jump("Hop Hop") . "<br>"."<br>";

$sungokong = new ape("kera sakti");
echo "Name: " . $sungokong->name . "<br>";
echo "legs: " . $sungokong->legs . "<br>";
echo "cool blooded: " . $sungokong->cool_blooded . "<br>";
echo "yell : " . $sungokong->yell("Auooo") . "<br>"."<br>";

/*
//menggunakan method get

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");

echo "Name: " . $sheep->getName(); // "shaun"
echo "legs: " . $sheep->getLegs(); // 2
echo "cool blooded: " . $sheep->getCold_Blooded(); // false

echo "<br>";
$kodok = new Frog("buduk");
echo "Name: " . $kodok->getName(); // "kera sakti"
echo "legs: " . $kodok->getLegs(); // 4
echo "cool blooded: " . $kodok->getCold_Blooded(); // false
echo "jump : " . $kodok->jump("Hop Hop") ; // "hop hop"

echo "<br>"."<br>";
$sungokong = new Ape("kera sakti");
echo "Name: " . $sungokong->getName(); // "kera sakti"
echo "legs: " . $sungokong->getLegs(4); // 4
echo "cool blooded: " . $sungokong->getCold_Blooded(); // false
echo "yell : " . $sungokong->yell("Auooo"); // "Auooo"
 */
?>