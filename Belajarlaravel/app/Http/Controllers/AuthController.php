<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Register(){
        return view('halaman.Register');
    }
    public function Welcome(Request $request){
        $fullname = $request['firstname'] ." ". $request['lastname'];
        $jeniskelamin = $request['jk'];
        return view('halaman.Welcome', ['fullname' => $fullname , 'jeniskelamin' => $jeniskelamin]);
    }
}
