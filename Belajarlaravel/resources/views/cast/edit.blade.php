@extends('layout.master')
@section('title')
Halaman edit Cast    
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
  @csrf
  @method('put')
    <div class="form-group">
      <label>Nama Cast</label>
      <input name="nama" type="text" value="{{old('nama', $cast->nama)}}" class="form-control">
    </div>
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Umur Cast</label>
        <input name="umur" type="number" value="{{old('umur', $cast->umur)}}" class="form-control">
      </div>
          @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Bio Cast</label>
      <textarea name="bio" class="form-control" cols="30" rows="10">{{old('bio', $cast->nama)}}</textarea>
    </div>
        @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection