@extends('layout.master')
@section('title')
Buat Account Baru!    
@endsection

@section('content')
<h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="firstname"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="lastname"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" value="1" name="jk">Male<br>
        <input type="radio" value="2" name="jk">Female<br>
        <input type="radio" value="3" name="jk">Other<br><br>
        <label>Nationality:</label><br><br>
        <select name="Nationality">
        <option value="Indonesian">Indonesian</option>
        <option value="Singaporean">Singaporean</option>
        <option value="Malaysian">Malaysian</option>
        <option value="Australian">Australian</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="language">Bahasa indonesia<br>
        <input type="checkbox" name="language">English<br>
        <input type="checkbox" name="language">Other<br><br>
        <label>Bio:</label><br><br>
        <textarea name="message" rows="10" cols="30"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>    
@endsection   