<?php

use Illuminate\Support\Facades\Route;
use App\http\controllers\HomeController;
use App\http\controllers\AuthController;
use App\http\controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class,'Home']);
Route::get('/register', [AuthController::class,'Register']);
Route::post('/welcome', [AuthController::class,'Welcome']);

Route::get('/table' , function(){
    return view('halaman.table');
});
Route::get('/data-table' , function(){
    return view('halaman.data-table');
});


//CRUD Cast
//menampilkan list data para pemain film
Route::get('/cast', [CastController::class, 'index'] );
//menampilkan form untuk membuat data pemain film baru
Route::get('/cast/create', [CastController::class, 'create'] );
//menyimpan data baru ke tabel Cast
Route::post('/cast', [CastController::class, 'store'] );
//menampilkan detail data pemain film dengan id tertentu
Route::get('/cast/{cast_id}', [CastController::class, 'show'] );
//menampilkan form untuk edit pemain film dengan id tertentu
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit'] );
//menyimpan perubahan data pemain film (update) untuk id tertentu
Route::put('/cast/{cast_id}', [CastController::class, 'update'] );
//menghapus data pemain film dengan id tertentu
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy'] );
